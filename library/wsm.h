#ifndef __WSM_H
#define __WSM_H

/*  
	Copyright Notice:
	Richard I. Boaz
	2012 - All rights reserved
*/

#define WSM_VERSION "0.1"

typedef enum {				// UI display states
	WSM_UISTATE_DBNOCONN,	// No DB Connection
	WSM_UISTATE_DBDISPLAY,	// DB Info Display
	WSM_UISTATE_DBEDIT,		// DB Info Edit
	WSM_UISTATE_DBCREATE,	// DB Create
	WSM_UISTATE_TTL
} WSM_UI_STATE;

typedef enum {				// widget state settings
  WSM_FALSE,					// FALSE, OFF, etc.
  WSM_TRUE,					// TRUE, ON, etc.
  WSM_TF_TTL
} WSM_TF;

// WSM Function Declares - externally available
void      wsm_set_UIState(WSM_UI_STATE /* uiState */);
void wsm_assign_sensitive(WSM_UI_STATE /* uiState */, WSM_TF /* wSetting */, GtkWidget* /* widget */);
void   wsm_assign_visible(WSM_UI_STATE /* uiState */, WSM_TF /* wSetting */, GtkWidget* /* widget */);
void  wsm_assign_editable(WSM_UI_STATE /* uiState */, WSM_TF /* wSetting */, GtkWidget* /* widget */);
void wsm_remove_sensitive(WSM_UI_STATE /* uiState */, WSM_TF /* wSetting */, GtkWidget* /* widget */);
void   wsm_remove_visible(WSM_UI_STATE /* uiState */, WSM_TF /* wSetting */, GtkWidget* /* widget */);
void  wsm_remove_editable(WSM_UI_STATE /* uiState */, WSM_TF /* wSetting */, GtkWidget* /* widget */);

#endif
