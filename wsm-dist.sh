#!/bin/bash

BASEDIR=`pwd`
SRCDIR=$BASEDIR
DISTRODIRNAME=WSM-distro
DISTRODIR=$BASEDIR/$DISTRODIRNAME

/bin/rm -r $DISTRODIR 2>/dev/null
mkdir $DISTRODIR
dirsS='library'
dirsD='library docs example'

# first clean the source directories
for dir in $dirsS ; do
	cd $SRCDIR/$dir
	make clean
done

# next delete the current distro directory and re-create
for dir in $dirsD ; do
	mkdir $DISTRODIR/$dir
done

# per directory, copy the list of files to be distributed
dir=library
files='makefile wsm.c wsm.h wsm_internal.h'
for file in $files ; do
	cp $SRCDIR/$dir/$file $DISTRODIR/$dir
done

dir=docs
files='WSM.pdf'
for file in $files ; do
	cp $SRCDIR/$dir/$file $DISTRODIR/$dir
done

dir=example
files='wsm-example.c'
for file in $files ; do
	cp $SRCDIR/$dir/$file $DISTRODIR/$dir
done

cp $SRCDIR/README.txt $DISTRODIR

# create the zip file for upload
zipF=WSM.zip
cd $BASEDIR
/bin/rm $zipF 2>/dev/null
zip -r $zipF $DISTRODIRNAME

