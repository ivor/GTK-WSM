#include <gtk/gtk.h>
#include "wsm.h"
#include "wsm_internal.h"

static gboolean inited;
static GPtrArray  *wsmState[WSM_UISTATE_TTL][WSM_WSTATE_TTL][WSM_TF_TTL];		// widget pointer arrays
static void	*settingFuncs[WSM_WSTATE_TTL];									// funcs for WSM_WSTATE enums
static gboolean settingV[WSM_TF_TTL] = {FALSE, TRUE};							// WSM_TF - gboolean equiv's

// WSM internal functions to do the actual seting of a widget's state
static void _wsm_setSensitive(GtkWidget *widget, gpointer s)
{
	gboolean sensState = (gboolean) GPOINTER_TO_INT(s);
	gtk_widget_set_sensitive(widget, sensState);
}

static void _wsm_setVisible(GtkWidget *widget, gpointer v)
{
	gboolean visState = (gboolean) GPOINTER_TO_INT(v);
	visState ? gtk_widget_show(widget) : gtk_widget_hide(widget);
}

static void _wsm_setEditable(GtkWidget *widget, gpointer e)
{
	gboolean editState = (gboolean) GPOINTER_TO_INT(e);
	gtk_editable_set_editable(GTK_EDITABLE(widget), editState);
}

// WSM Library initialization - internalized and guaranteed
static inline void _wsm_init()
{
	int i, j, k;

	settingFuncs[WSM_WSTATE_SENSITIVE] = _wsm_setSensitive;
	settingFuncs[WSM_WSTATE_VISIBLE] = _wsm_setVisible;
	settingFuncs[WSM_WSTATE_EDITABLE] = _wsm_setEditable;
	
	// make our display state pointer arrays to hold our widgets
	for (i=0;i<WSM_UISTATE_TTL;i++)
		for (j=0;j<WSM_WSTATE_TTL;j++)
			for (k=0;k<WSM_TF_TTL;k++)
				wsmState[i][j][k] = g_ptr_array_new();
}

// function to add and remove widgets to/from our state array - internal version
static inline void _wsm_command(WSM_CMD wsmCmd, GtkWidget *widget, 
								WSM_UI_STATE uiState, WSM_TF wSetting, WSM_WSTATE wState)
{
	WSM_INITED()
	if (!WSM_VALID_DATA())
		return;
	
	switch(wsmCmd)
	{
		case WSM_CMD_ASSIGN:
			g_ptr_array_add(wsmState[uiState][wState][wSetting], widget);
		break;
		case WSM_CMD_REMOVE:
			while (g_ptr_array_remove(wsmState[uiState][wState][wSetting], widget));
		break;
		default:
		break;
	}
}

// all functions ABOVE this line are internal to the WSM library
//
// ====================================================================
//
// all functions BELOW this line are available to the outside world

// functions to remove widgets from our state array - external version
void wsm_remove_editable(WSM_UI_STATE uiState, WSM_TF wSetting, GtkWidget *widget)
{
	_wsm_command(WSM_CMD_REMOVE, widget, uiState, wSetting, WSM_WSTATE_EDITABLE);
}
void wsm_remove_visible(WSM_UI_STATE uiState, WSM_TF wSetting, GtkWidget *widget)
{
	_wsm_command(WSM_CMD_REMOVE, widget, uiState, wSetting, WSM_WSTATE_VISIBLE);
}
void wsm_remove_sensitive(WSM_UI_STATE uiState, WSM_TF wSetting, GtkWidget *widget)
{
	_wsm_command(WSM_CMD_REMOVE, widget, uiState, wSetting, WSM_WSTATE_SENSITIVE);
}

// functions to assign widgets to our state array - external version
void wsm_assign_editable(WSM_UI_STATE uiState, WSM_TF wSetting, GtkWidget *widget)
{
	_wsm_command(WSM_CMD_REMOVE, widget, uiState, wSetting, WSM_WSTATE_EDITABLE);
	_wsm_command(WSM_CMD_ASSIGN, widget, uiState, wSetting, WSM_WSTATE_EDITABLE);
}
void wsm_assign_visible(WSM_UI_STATE uiState, WSM_TF wSetting, GtkWidget *widget)
{
	_wsm_command(WSM_CMD_REMOVE, widget, uiState, wSetting, WSM_WSTATE_VISIBLE);
	_wsm_command(WSM_CMD_ASSIGN, widget, uiState, wSetting, WSM_WSTATE_VISIBLE);
}
void wsm_assign_sensitive(WSM_UI_STATE uiState, WSM_TF wSetting, GtkWidget *widget)
{
	_wsm_command(WSM_CMD_REMOVE, widget, uiState, wSetting, WSM_WSTATE_SENSITIVE);
	_wsm_command(WSM_CMD_ASSIGN, widget, uiState, wSetting, WSM_WSTATE_SENSITIVE);
}

void wsm_set_UIState(WSM_UI_STATE uiState)
{
	int i, j;
	
	WSM_INITED()
	if (!WSM_VALID_UISTATE(uiState))
		return;
	
	for (i = 0; i < WSM_WSTATE_TTL; i++)
		for (j=0; j < WSM_TF_TTL; j++)
			g_ptr_array_foreach(wsmState[uiState][i][j], (GFunc) settingFuncs[i], GINT_TO_POINTER(settingV[j]));
}
