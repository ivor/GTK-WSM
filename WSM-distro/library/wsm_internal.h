#ifndef __WSM_INTERNAL_H
#define __WSM_INTERNAL_H

typedef enum {				// widget states handled by the WSM
  WSM_WSTATE_SENSITIVE,		// sensitivity
  WSM_WSTATE_VISIBLE,		// visibility
  WSM_WSTATE_EDITABLE,		// editable
  WSM_WSTATE_TTL
} WSM_WSTATE;

typedef enum {
	WSM_CMD_REMOVE,
	WSM_CMD_ASSIGN,
	WSM_CMD_TTL
} WSM_CMD;

#define WSM_INITED()		if (!inited) {inited=TRUE;_wsm_init();}
#define WSM_VALID_DATA()	((wsmCmd >= 0 && wsmCmd < WSM_CMD_TTL) && \
							 (uiState >= 0 && uiState < WSM_UISTATE_TTL) && \
							 (wSetting >= 0 && wSetting < WSM_TF_TTL) && \
							 (wState >= 0 && wState < WSM_WSTATE_TTL))\
								? TRUE \
								: FALSE
#define WSM_VALID_UISTATE(s)	((s >= 0 && s < WSM_UISTATE_TTL))\
								? TRUE \
								: FALSE
	
#endif
