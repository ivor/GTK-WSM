WSM C Library source code:

Distribution Directories:
library - containing all source code and header files comprising the WSM library
example - containing a sample implementation of the WSM library
docs - containing the complete documentation set for the WSM library

To compile the WSM library:
sh> cd library
sh> make

Two files, wsm.h and wsm.a, will be copied to the upper-most directory of the distro

Complete documentation can be found in the docs directory.
